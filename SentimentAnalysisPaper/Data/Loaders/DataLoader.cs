﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;
using SentimentAnalysisPaper.Data.Interfaces;

namespace SentimentAnalysisPaper.Data.Loaders
{
    public class DataLoader : IDataLoader
    {
        private readonly string _filePath;
        private readonly SentimentRecordClassMap _classMap;

        public DataLoader(string filePath, SentimentRecordClassMap classMap)
        {
            _filePath = filePath;
            _classMap = classMap;
        }

        public IEnumerable<AirlineSentimentRecord> Load()
        {
            using (var reader = new CsvReader(new StreamReader(_filePath)))
            {
                reader.Configuration.RegisterClassMap(_classMap);
                var records = reader.GetRecords<AirlineSentimentRecord>();
                return records.ToList();
            }
        }
    }
}