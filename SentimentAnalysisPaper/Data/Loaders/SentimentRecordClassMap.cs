﻿using System.Collections.Generic;
using System.Linq;
using CsvHelper.Configuration;
using SentimentAnalysisPaper.Processing.Preprocessors;
using SentimentAnalysisPaper.Processing.TextPreprocessors;

namespace SentimentAnalysisPaper.Data.Loaders
{
    public sealed class SentimentRecordClassMap : ClassMap<AirlineSentimentRecord>
    {
        public SentimentRecordClassMap(IEnumerable<ITextPreprocessor> textPreprocessors, ISentimentMapper sentimentMapper)
        {
            Map(m => m.Sentiment).ConvertUsing(readerRow =>
            {
                var text = readerRow.GetField("sentiment");
                return sentimentMapper.Map(text);

            });

            Map(m => m.Text).ConvertUsing(readerRow =>
            {
                var text = readerRow.GetField("text").ToLower();
                var preprocessed = textPreprocessors.Aggregate(text, (current, textPreprocessor) => textPreprocessor.Process(current));
                return preprocessed;
            });
        }
    }
}