﻿using SentimentAnalysisPaper.Data.Interfaces;
using SentimentAnalysisPaper.Processing.Preprocessors;
using SentimentAnalysisPaper.Processing.TextPreprocessors;

namespace SentimentAnalysisPaper.Data.Loaders
{
    public class DataLoaderFactory
    {
        private readonly string _dataFilePath;

        public DataLoaderFactory(string dataFilePath)
        {
            _dataFilePath = dataFilePath;
        }

        public IDataLoader GetLoader()
        {
            var textPreprocessors = new ITextPreprocessor[]
            {
                new UrlsPreprocessor(),
                new EmojiPreprocessor(),
                new HashtagAndMentionTextPreprocessor(),
                new DuplicatesNumbersAndPunctuationPreprocessor(),
            };
            var sentimentMapper = new SentimentToEnumMapper();

            var classMap = new SentimentRecordClassMap(textPreprocessors, sentimentMapper);
            return new DataLoader(_dataFilePath, classMap);
        }
    }
}