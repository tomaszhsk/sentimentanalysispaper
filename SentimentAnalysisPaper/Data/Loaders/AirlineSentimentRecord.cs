﻿using SentimentAnalysisPaper.Data.Enums;

namespace SentimentAnalysisPaper.Data.Loaders
{
    public class DataSplitter
    {
    }

    public class AirlineSentimentRecord
    {
        public Sentiment Sentiment { get; set; }

        public string Text { get; set; }
    }
}