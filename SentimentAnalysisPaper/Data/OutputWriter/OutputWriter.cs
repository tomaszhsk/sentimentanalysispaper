﻿using System;
using System.Globalization;
using System.IO;
using System.Reflection;

namespace SentimentAnalysisPaper.Data.OutputWriter
{
    public class OutputWriter
    {
        public void WriteToConsole(string modelName, double accuracy)
        {
            Console.WriteLine("====================================== Prediction complete ================================");
            Console.WriteLine($"{modelName}: {accuracy}");
        }

        public void WriteToFile(double accuracy, string[] messages, string[] testDataPredictions, string[] testDataTargets)
        {
            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            using (TextWriter tw = new StreamWriter($"{path}\\{DateTime.Now:yyyyMMdd_hhmmss}_output.txt"))
            {
                tw.WriteLine("====================================== Prediction complete ================================");
                tw.WriteLine($"Model accuracy: {accuracy}");
                for (var index = 0; index < testDataPredictions.Length; index++)
                {
                    var testMessage = messages[index];
                    var prediction = testDataPredictions[index];
                    var target = testDataTargets[index];
                    tw.WriteLine($"{testMessage} | prediction: {prediction} | target: {target}");
                }
            }
        }
    }
}