﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SentimentAnalysisPaper.Data.DataProcessing
{
    class DataSplitter
    {
        public (IEnumerable<T> testData, IEnumerable<T> trainingData) Split<T>(List<T> data, double testFraction)
        {
            var dataCount = data.Count;
            var fractionIndex = (int) Math.Abs(dataCount * testFraction);

            var testData = data.Take(fractionIndex);
            var trainingData = data.Skip(fractionIndex);

            var tuple = (testData: testData, trainingData: trainingData);
            return tuple;
        }
    }
}