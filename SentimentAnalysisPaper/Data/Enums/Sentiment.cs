﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SentimentAnalysisPaper.Data.Enums
{
    public enum Sentiment
    {
        Negative = -1,
        Neutral = 0,
        Positive = 1
    }
}
