﻿using System.Collections.Generic;
using SentimentAnalysisPaper.Data.Loaders;

namespace SentimentAnalysisPaper.Data.Interfaces
{
    public interface IDataLoader
    {
        IEnumerable<AirlineSentimentRecord> Load();
    }
}