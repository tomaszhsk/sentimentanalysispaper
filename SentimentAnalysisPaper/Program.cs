﻿using System;
using System.Linq;
using SentimentAnalysisPaper.Data.Loaders;
using SentimentAnalysisPaper.Data.OutputWriter;
using SentimentAnalysisPaper.Processing.Classifiers;
using SentimentAnalysisPaper.Processing.Vectorizers.TFIDF;
using DataSplitter = SentimentAnalysisPaper.Data.DataProcessing.DataSplitter;

namespace SentimentAnalysisPaper
{
    class Program
    {

        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Path to a file with data is missing. Please eneter a path to a file with data to process");
                return;
            }

            var dataLoaderFactory = new DataLoaderFactory(args[0]);
            var airlinesDataLoader = dataLoaderFactory.GetLoader();
            var airlinesData = airlinesDataLoader.Load().ToList();

            var dataSplitter = new DataSplitter();
            var testFraction = 0.2;
            var (testData, trainingData) = dataSplitter.Split(airlinesData, testFraction);
            var trainingDataList = trainingData.ToList();
            var testDataList = testData.ToList();

            var trainingMessages = trainingDataList.Select(n => n.Text).ToArray();
            var trainingTargets = trainingDataList.Select(n =>
            {
                var intValue = (int) n.Sentiment;
                return intValue.ToString();
            }).ToArray();


            var testMessages = testDataList.Select(n => n.Text).ToArray();
            var testTargets = testDataList.Select(n =>
            {
                var intValue = (int) n.Sentiment;
                return intValue.ToString();
            }).ToArray();

            //learn features
            var vectorizer = new TfidfVectorizer();
            var trainingFeatures = vectorizer.FitTransform(trainingMessages);
            var testFeatures = vectorizer.Transform(testMessages);

            var classifier = new Sharpkit.Learn.Tree.DecisionTreeClassifier<string>(maxDepth: 3);
            classifier.Fit(trainingFeatures, trainingTargets);

            var testDataPrediction = classifier.Predict(testFeatures);

            var randomClassifier = new RandomClassifier();
            var randomPrediction = randomClassifier.Predict(testTargets); 

            var classifierAccuracyScore = Sharpkit.Learn.Metrics.Metrics.AccuracyScore(testTargets, testDataPrediction);
            var randomClassifierAccuracyScore = Sharpkit.Learn.Metrics.Metrics.AccuracyScore(testTargets, randomPrediction);

            var outputWriter = new OutputWriter();
            outputWriter.WriteToConsole("Decission tree classifier accuracy", classifierAccuracyScore);
            outputWriter.WriteToConsole("Random classifier accuracy", randomClassifierAccuracyScore);
            outputWriter.WriteToFile(classifierAccuracyScore, testMessages, testDataPrediction, testTargets);

            Console.ReadLine(); //awaits enter
        }


    }
}
