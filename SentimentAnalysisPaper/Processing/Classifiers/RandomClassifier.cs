﻿using System;
using System.Linq;

namespace SentimentAnalysisPaper.Processing.Classifiers
{
    internal class RandomClassifier
    {
        public string[] Predict(string [] testTargets)
        {
            var rand = new Random();
            var sentiments = new[] { "-1", "0", "1" };
            var randomPredictions = testTargets.Select(x => sentiments[rand.Next(sentiments.Length)]).ToArray();
            return randomPredictions;
        }
    }
}