﻿using System;
using System.Linq;

namespace SentimentAnalysisPaper.Processing.Vectorizers.TFIDF
{
    public class VectorNormalizer
    {
        /// <summary>
        /// Normalizes a TF*IDF array of vectors using L2-Norm.
        /// Xi = Xi / Sqrt(X0^2 + X1^2 + .. + Xn^2)
        /// </summary>
        /// <param name="vectors">double[][]</param>
        /// <returns>double[][]</returns>
        public double[][] Normalize(double[][] vectors)
        {
            // Normalize the vectors using L2-Norm.
            return vectors.Select(Normalize).ToArray<double[]>();
        }

        /// <summary>
        /// Normalizes a TF*IDF vector using L2-Norm.
        /// Xi = Xi / Sqrt(X0^2 + X1^2 + .. + Xn^2)
        /// </summary>
        /// <param name="vectors">double[][]</param>
        /// <returns>double[][]</returns>
        public double[] Normalize(double[] vector)
        {
            var sumSquared = vector.Sum(value => value * value);

            var sqrtSumSquared = Math.Sqrt(sumSquared);

            return vector.Select(value => value / sqrtSumSquared).ToArray();
        }
    }
}