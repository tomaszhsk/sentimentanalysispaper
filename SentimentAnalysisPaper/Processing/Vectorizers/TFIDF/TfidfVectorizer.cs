﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using EnglishStemmer;
using MathNet.Numerics.LinearAlgebra.Double;
using TFIDF;

namespace SentimentAnalysisPaper.Processing.Vectorizers.TFIDF
{
    public class TfidfVectorizer
    {
        private Dictionary<string, double> _vocabularyIDF = new Dictionary<string, double>();
        private readonly StringTokenizer _stringTokenizer = new StringTokenizer();

        public SparseMatrix FitTransform(string[] documents, int vocabularyThreshold = 3)
        {
            var vocabulary = StemDocsAndGetVocabulary(documents, out var stemmedDocs, vocabularyThreshold);

            ResetIdfVocabulary();

            var stemmedDocsCount = stemmedDocs.Count;
            CalcuateIDF(vocabulary, stemmedDocs, stemmedDocsCount);

            var vectors = TransformToTFIDFVectors(stemmedDocs, _vocabularyIDF);

            var vocabularyIdfCount = _vocabularyIDF.Count;
            //x [ x: nSamples, y: nFeatures]
            // samples - documents - rows
            // features - vocabulary - columns
            return SparseMatrix.OfRows(stemmedDocsCount, vocabularyIdfCount, vectors);
        }

        /// <summary>
        /// Transforms a list of documents into their associated TF*IDF values.
        /// </summary>
        /// <param name="documents">string[]</param>
        /// <param name="vocabularyThreshold">Minimum number of occurences of the term within all documents</param>
        /// <returns>double[][]</returns>
        public SparseMatrix Transform(string[] documents, int vocabularyThreshold = 3)
        {
            //here using it only to stem docs
            StemDocsAndGetVocabulary(documents, out var stemmedDocs, vocabularyThreshold);

            var vectors = TransformToTFIDFVectors(stemmedDocs, _vocabularyIDF);
            return SparseMatrix.OfRows(stemmedDocs.Count, _vocabularyIDF.Count, vectors);
        }

        private void CalcuateIDF(List<string> vocabulary, List<List<string>> stemmedDocs, int stemmedDocsCount)
        {
            foreach (var term in vocabulary)
            {
                double numberOfDocsContainingTerm = stemmedDocs.Count(d => d.Contains(term));
                _vocabularyIDF[term] = Math.Log(stemmedDocsCount / (1 + numberOfDocsContainingTerm));
            }
        }

        private void ResetIdfVocabulary()
        {
            if (_vocabularyIDF.Count > 0)
                _vocabularyIDF = new Dictionary<string, double>();
        }

        /// <summary>
        /// Converts a list of stemmed documents (lists of stemmed words) and their associated vocabulary + idf values, into an array of TF*IDF values.
        /// </summary>
        /// <param name="stemmedDocs">List of List of string</param>
        /// <param name="vocabularyIDF">Dictionary of string, double (term, IDF)</param>
        /// <returns>double[][]</returns>
        private List<List<double>> TransformToTFIDFVectors(List<List<string>> stemmedDocs, Dictionary<string, double> vocabularyIDF)
        {
            var vectors = new List<List<double>>();
            foreach (var doc in stemmedDocs)
            {
                var vector = new List<double>();

                foreach (var vocab in vocabularyIDF)
                {
                    // Term frequency = count how many times the term appears in this document.
                    var tf = doc.Count(d => d == vocab.Key);
                    var tfidf = tf * vocab.Value;

                    vector.Add(tfidf);
                }

                vectors.Add(vector);
            }

            return vectors;
        }

        /// <summary>
        /// Parses and tokenizes a list of documents, returning a vocabulary of words.
        /// </summary>
        /// <param name="docs">string[]</param>
        /// <param name="stemmedDocs">List of List of string</param>
        /// <returns>Vocabulary (list of strings)</returns>
        private List<string> StemDocsAndGetVocabulary(string[] docs, out List<List<string>> stemmedDocs, int vocabularyThreshold)
        {
            var wordCountList = new Dictionary<string, int>();
            stemmedDocs = new List<List<string>>();

            var docIndex = 0;
            foreach (var doc in docs)
            {
                docIndex++;
                var stemmedDoc = new List<string>();
                if (docIndex % 100 == 0)
                    Console.WriteLine("Processing " + docIndex + "/" + docs.Length);

                var tokenizedWords = _stringTokenizer.Tokenize(doc);
                foreach (var word in tokenizedWords)
                {
                    // Strip non-alphanumeric characters.
                    var strippedWord = Regex.Replace(word, "[^a-zA-Z0-9]", "");

                    if (StopWords.stopWordsList.Contains(strippedWord.ToLower()))
                        continue;

                    var englishWord = new EnglishWord(strippedWord);
                    var stem = englishWord.Stem;

                    if (stem.Length <= 0)
                        continue;

                    if (wordCountList.ContainsKey(stem))
                    {
                        wordCountList[stem]++;
                    }
                    else
                    {
                        wordCountList.Add(stem, 0);
                    }

                    stemmedDoc.Add(stem);
                }

                stemmedDocs.Add(stemmedDoc);
            }

            // Get the top words.
            var vocabList = wordCountList.Where(w => w.Value >= vocabularyThreshold);
            return vocabList.Select(item => item.Key).ToList();
        }
    }
}
