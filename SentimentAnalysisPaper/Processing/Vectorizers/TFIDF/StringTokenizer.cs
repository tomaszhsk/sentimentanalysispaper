﻿using System.Text.RegularExpressions;

namespace SentimentAnalysisPaper.Processing.Vectorizers.TFIDF
{
    public class StringTokenizer
    {
        /// <summary>
        /// Tokenizes a string, returning its list of words.
        /// </summary>
        /// <param name="text">string</param>
        /// <returns>string[]</returns>
        public string[] Tokenize(string text)
        {
            // Tokenize and also get rid of any punctuation
            return text.Split(" @$/#.-:&*+=[]?!(){},''\">_<;%\\".ToCharArray());
        }
    }
}