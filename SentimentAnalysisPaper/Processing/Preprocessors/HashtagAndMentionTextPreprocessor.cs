﻿using System.Text.RegularExpressions;
using SentimentAnalysisPaper.Processing.TextPreprocessors;

namespace SentimentAnalysisPaper.Processing.Preprocessors
{
    public class HashtagAndMentionTextPreprocessor : ITextPreprocessor
    {
        private readonly string _mentionRegex = "[\\s]*([@][\\w_-]+)";
        private readonly string _hashTagRegex = "[\\s]*([#][\\w_-]+)";



        public string Process(string text)
        {
            text = Regex.Replace(text, _mentionRegex, "username").ToLower();
            text = Regex.Replace(text, _hashTagRegex, " ").ToLower();
            return text;
        }
    }
}
