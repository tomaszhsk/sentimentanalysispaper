﻿using System.Text.RegularExpressions;
using SentimentAnalysisPaper.Processing.TextPreprocessors;

namespace SentimentAnalysisPaper.Processing.Preprocessors
{
    public class UrlsPreprocessor : ITextPreprocessor
    {
        private readonly string _urlsRegex = @"(http|https):\/\/[^\s]*";
            
        public string Process(string text)
        {
            return Regex.Replace(text, _urlsRegex, " ");
        }
    }
}