﻿namespace SentimentAnalysisPaper.Processing.TextPreprocessors
{
    public interface ITextPreprocessor
    {
        string Process(string text);
    }
}