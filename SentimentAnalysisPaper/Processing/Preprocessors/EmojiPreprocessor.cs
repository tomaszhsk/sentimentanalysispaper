﻿using System;
using System.IO;
using System.Threading.Tasks;
using NeoSmart.Unicode;
using SentimentAnalysisPaper.Processing.TextPreprocessors;

namespace SentimentAnalysisPaper.Processing.Preprocessors
{
    public class EmojiPreprocessor : ITextPreprocessor
    {
        private string DirectoryPath => Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
        private string EmojiMappingFile => $@"{DirectoryPath}\Data\emoji_mapping.txt";
        public string Process(string text)
        {

            foreach (var singleEmoji in Emoji.All)
            {
                text = text.Replace(singleEmoji.Sequence.AsString, $" {singleEmoji.Name} ");
            }

            return text;
        }
    }
}