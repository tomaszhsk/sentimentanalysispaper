﻿using System.Text.RegularExpressions;
using SentimentAnalysisPaper.Processing.TextPreprocessors;

namespace SentimentAnalysisPaper.Processing.Preprocessors
{
    public class DuplicatesNumbersAndPunctuationPreprocessor : ITextPreprocessor
    {
        private readonly string _numbersRegex = "[0-9]+";
        private readonly string _duplicateCharactersRegex = "([a-zA-Z])\\1+";
        private readonly string _punctuationRegex = @"[\p{P}-[()\-.]]";

        public string Process(string text)
        {
            //var singleCharacter = Regex.Replace(text, _duplicateCharactersRegex, "$1");
            text = Regex.Replace(text, _numbersRegex, " ");
            text = Regex.Replace(text, _punctuationRegex, "");
            return text;
        }
    }
}