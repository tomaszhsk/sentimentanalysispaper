﻿using SentimentAnalysisPaper.Data.Enums;

namespace SentimentAnalysisPaper.Processing.Preprocessors
{
    public interface ISentimentMapper
    {
        Sentiment Map(string text);
    }
}