﻿using SentimentAnalysisPaper.Data.Enums;
using SentimentAnalysisPaper.Processing.TextPreprocessors;

namespace SentimentAnalysisPaper.Processing.Preprocessors
{
    public class SentimentToEnumMapper : ISentimentMapper
    {
        public Sentiment Map(string text)
        {
            switch (text)
            {
                case "negative":
                    return Sentiment.Negative;
                case "positive":
                    return Sentiment.Positive;
                default:
                    return Sentiment.Neutral;
            }
        }
    }
}