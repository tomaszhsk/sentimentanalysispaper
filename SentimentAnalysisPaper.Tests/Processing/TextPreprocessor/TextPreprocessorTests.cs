﻿using System.Linq.Expressions;
using System.Net;
using NUnit.Framework;
using SentimentAnalysisPaper.Processing.Preprocessors;

namespace SentimentAnalysisPaper.Tests.Processing.TextPreprocessor
{
    [TestFixture()]
    class TextPreprocessorTests
    {
        [Test]
        [TestCase("@VirginAmerica terrible flight #theworst", " terrible flight")]
        [TestCase("@VirginAmerica @josh terrible flight #theworst #terrible", " terrible flight")]
        [TestCase("Oh my god @VirginAmerica @josh terrible flight #theworst #terrible", "Oh my god terrible flight")]
        public void CleanData_ShouldRemoveHashtagsAndMentions(string input, string expected)
        {
            //arrange
            var preprocessor = new HashtagAndMentionTextPreprocessor();
            
            //act
            var result = preprocessor.Process(input);
            
            //assert
            Assert.AreEqual(expected, result);
        }


        [Test]
        [TestCase(@"i ❤️ flying", "i red heart flying")]
        public void EmojiPreprocessor_ShouldReplaceEmojisWithText(string input, string expected)
        {
            //arrange
            var emojiPreprocessor = new EmojiPreprocessor();

            //act
            var result = emojiPreprocessor.Process(input);

            //assert
            Assert.AreEqual(expected, result);
        }

        [Test]
        [TestCase("I love this graphic. http://t.co/UT5GrRwAaA", "I love this graphic.  ")]
        [TestCase("America could start flights to Hawaii by end of year http://t.co/r8p2Zy3fe4 via @Pacificbiznews", "America could start flights to Hawaii by end of year   via @Pacificbiznews")]
        public void UrlPreprocessor_ShouldReplaceUrl_WithEmptyString(string input, string expected)
        {
            //arrange
            var urlsPreprocessor = new UrlsPreprocessor();

            //act
            var result = urlsPreprocessor.Process(input);

            //assert
            Assert.AreEqual(expected, result);
        }
    }
}
